import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { App }   from './app.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { Home } from './home';

@NgModule({
  imports:      [ BrowserModule,  RouterModule.forRoot(ROUTES) ],
  declarations: [ App, Home ],
  bootstrap:    [ App ]
})
export class AppModule { }
