var webpack = require('webpack');
var helpers = require('./helpers');

//Plugins
const CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlElementsPlugin = require('./html-elements-plugin');

const METADATA = {
    title: 'Angular Starter Project',
    description: 'Angular Starter Project',
    baseUrl: '/'
};

module.exports = {
    metadata: METADATA,

    entry: {
        'polyfills': './src/polyfills.browser.ts',
        'vendor': './src/vendor.browser.ts',
        'app': './src/main.browser.ts'
    },
    resolve: {
        extensions: ['', '.ts', '.js', '.css', '.scss'],

        // ensure root is src
        root: helpers.root('src'),

        // remove other default values
        modulesDirectories: ['node_modules', 'bower_components']

    },
    module: {
        preLoaders: [{
                test: /\.ts$/,
                loader: 'tslint-loader',
                exclude: [helpers.root('node_modules')]
            },

            {
                test: /\.js$/,
                loader: 'source-map-loader',
                exclude: [
                    // Below packages have known problems with their sourcemaps
                    helpers.root('node_modules/rxjs')
                ]
            }
        ],
        loaders: [
            // Webpack is responsible for bundling your application files. It can bundle any kind of file: JavaScript, TypeScript, CSS, SASS, LESS, images, html, fonts, whatever. 
            // However Webpack itself doesn't know what to do with a non-JavaScript file. We teach it to process such files into JavaScript with loaders. As shown below


            // This loads all Typescript files
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'angular2-template-loader']
            },

            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            // This loads ALL html templates
            {
                test: /\.html$/,
                loader: 'raw-loader',
                exclude: [helpers.root('src/index.html')]
            },
            // This loads all image and font assets throughout the entire application
            {
                test: /\.(png|jpe?g|gif|woff|ico)$/,
                loader: 'file?name=assets/[name].[hash].[ext]'
            }, {
                test: /\.woff(2)?(\?v=.+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },

            {
                test: /\.(ttf|eot|svg)(\?v=.+)?$/,
                loader: 'file-loader'
            },
            // This loads all CSS
            {
                test: /\.css$/,
                loader: 'raw-loader'
            },
            // This loads all SASS that is on the src or app level an therefore is application wide CSS
            {
                test: /\.scss$/,
                include: helpers.root('src', 'app'),
                loaders: ['raw-loader', 'sass-loader']
            },
            // This loads all SASS that is contained within specific Components
            {
                test: /\.scss$/,
                exclude: helpers.root('src', 'app'),
                loaders: ['raw-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        // Causing an issue with webpack when loading
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: ['app', 'vendor', 'polyfills']
        // }),

        new HtmlWebpackPlugin({
            template: 'src/index.html',
            chunksSortMode: 'dependency'
        }),


        new CopyWebpackPlugin([{
            from: 'src/assets',
            to: 'assets'
        }]),
        // TODO: make sure this is a correct configuration
        new webpack.ProvidePlugin({
             jQuery: 'jquery',
             $: 'jquery',
             jquery: 'jquery',
            'Tether': 'tether',
            'window.Tether': 'tether'
        })
    ]
};