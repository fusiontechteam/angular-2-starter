# Starter Framework for Angular 2 Based projects

## Based on
Angular 2, Bootstrap 3, Webpack and lots of awesome modules and plugins

## Features
* TypeScript
* Webpack
* Bootstrap 3 CSS Framework
* Sass
* Angular 2
* jQuery


##License
[MIT](LICENSE.txt) license.


##Removed excess dependencies
I have removed excess dependencies from the angular-2-webpck-starter project hosted here (https://github.com/AngularClass/angular2-webpack-starter)

    "@angularclass/conventions-loader"
    "@angularclass/request-idle-callback"
    "@angularclass/webpack-toolkit"
    "assets-webpack-plugin"
    "http-server"
    "ie-shim"

    "exports-loader": "^0.6.3",
    "expose-loader": "^0.7.1",
    "gh-pages": "^0.11.0",
    "imports-loader": "^0.6.5",
    "istanbul-instrumenter-loader": "^0.2.0",
    "json-loader": "^0.5.4",
    "parse5": "^1.3.2",
    "phantomjs": "^2.1.7",
    "protractor": "^3.2.2",
    "string-replace-loader": "1.0.5",
    "to-string-loader": "^1.1.4",
    "ts-node": "^1.3.0",
    "typedoc": "^0.4.5",